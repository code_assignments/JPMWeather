package com.example.jpmweatherapp

import android.app.Application
import com.example.jpmweatherapp.ui.WeatherViewModel
import com.example.jpmweatherapp.util.http.SimpleHttpClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class MainApplication : Application(){
    override fun onCreate() {
        super.onCreate()

        val appModule = module {
            viewModel { WeatherViewModel(get()) }
            single{  SimpleHttpClient("d21f427ef1259f0980363161e207e8a1") }
        }

        startKoin{
            modules( appModule )
        }
    }
}