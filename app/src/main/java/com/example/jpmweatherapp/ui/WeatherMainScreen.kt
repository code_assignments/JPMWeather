package com.example.jpmweatherapp.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Search
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.example.jpmweatherapp.R

@Composable
fun appMainScreen(
    weatherViewModel: WeatherViewModel
) {
    val locationSearchRequested = remember { mutableStateOf(false) }
    val modifier: Modifier = Modifier
    val widgetsState by weatherViewModel.widgetsState.collectAsState()

    Column(
        modifier = modifier.padding(16.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    )
    {
        Text(
            stringResource(R.string.appTitle), modifier = modifier.fillMaxWidth(),
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp,
            color = Color(90, 86, 216, 255),
            textAlign = TextAlign.Center
        )
        cityLookUpBar(
            cityName = weatherViewModel.cityStateName,
            onCityNameChanged = { weatherViewModel.updateCityName(it) },
            onReadyToSearch = { weatherViewModel.getCityWeather(locationSearchRequested.value) },
            isEnabled = !locationSearchRequested.value
        )

        Row(
            modifier = Modifier
                .fillMaxWidth()
                // .border(1.dp, Color.Blue, RoundedCornerShape(2))
                .padding(top = 2.dp),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        ) {

            if (widgetsState.locationSearchEnabled) {
                Checkbox(
                    checked = locationSearchRequested.value,
                    onCheckedChange = {
                        locationSearchRequested.value = it
                        weatherViewModel.clearWeatherInfo()
                    }
                )
                Text(
                    text = "Search by current location", textAlign = TextAlign.Center
                )
            }
        }

        Spacer(modifier = Modifier.size(8.dp))

        Row(
            modifier = modifier
                .wrapContentHeight()
                .background(Color(199, 235, 240, 255))
                .fillMaxWidth()
                .border(1.dp, Color.Blue, RoundedCornerShape(2))
        )
        {

            temperatureAndWeatherPanel(
                widgetsState.currentTemperature,
                widgetsState.weatherIconID,
                widgetsState.weatherType
            )

            Column(modifier = modifier.padding(top = 24.dp)) {
                weatherDetailCard(modifier, "Humidity, %", "${widgetsState.humidity}")
                weatherDetailCard(modifier, "Pressure, hPa", "${widgetsState.pressure}")
                weatherDetailCard(
                    modifier,
                    "Wind, m/s",
                    "${widgetsState.windDir} ${widgetsState.windSpeed}"
                )
                weatherDetailCard(modifier, "Details", "${widgetsState.weatherDetails}")
                if (locationSearchRequested.value) {
                    weatherDetailCard(modifier, "City/place", "${widgetsState.cityName}")
                }
            }
        }

    }

    widgetsState.appAlert?.let {

        AlertDialog(
            onDismissRequest = { weatherViewModel.resetErrorMessage() },
            title = { Text(it.first) },
            text = { Text(it.second) },
            modifier = modifier,
            confirmButton = {
                TextButton({ weatherViewModel.resetErrorMessage() }) { Text(text = "Ok") }
            }
        )
    }
}


@Composable
fun weatherDetailCard(
    modifier: Modifier = Modifier,
    paramName: String,
    paramValue: String
) {

    Row(modifier = modifier.padding(start = 32.dp)) {
        Text("$paramName:", fontWeight = FontWeight.Bold)
        Text(
            paramValue,
            color = Color.Blue,
            modifier = modifier.padding(start = 6.dp, bottom = 6.dp)
        )
    }
}


@Composable
fun temperatureAndWeatherPanel(
    currentTemp: String,
    iconID: String,
    weatherType: String,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .padding(24.dp)

            .wrapContentSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    )
    {

        Text(
            "$currentTemp \u2103",
            fontWeight = FontWeight.Bold,
            fontSize = 40.sp,
            color = Color(143, 44, 160, 255),
            textAlign = TextAlign.Center
        )

        if (iconID.isNotBlank()) {
            AsyncImage(
                model = "https://openweathermap.org/img/wn/${iconID}@2x.png",
                contentDescription = null,
                placeholder = painterResource(R.drawable.circle_arrow)
            )
            Text(
                weatherType,
                fontWeight = FontWeight.Bold,
                fontSize = 20.sp,
                color = Color.Blue
            )
        }
    }
}


@Composable
fun cityLookUpBar(
    modifier: Modifier = Modifier,
    isEnabled: Boolean,
    cityName: String,
    onCityNameChanged: (String) -> Unit,
    onReadyToSearch: () -> Unit,
) {

    Row(
        modifier = modifier
            .fillMaxWidth()
            .wrapContentHeight()
    ) {

        OutlinedTextField(
            enabled = isEnabled,
            value = if (isEnabled) cityName else "",
            singleLine = true,
            modifier = Modifier.weight(1f),
            onValueChange = { onCityNameChanged(it) },
            label = { Text(stringResource(R.string.searchPrompt)) },
            keyboardOptions = KeyboardOptions.Default.copy(
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(
                onDone = { onReadyToSearch() }
            )
        )
        IconButton(
            onClick = { onReadyToSearch() },
            modifier = Modifier
                .weight(0.1f)
                .align(Alignment.CenterVertically)
                .padding(start = 8.dp)
        ) {
            Icon(Icons.Outlined.Search, contentDescription = "Search cities")
        }

    }
}