package com.example.jpmweatherapp.ui

data class UiWidgetState(

    val currentTemperature: String = "",
    val weatherType: String = "",
    val weatherDetails: String = "",
    val weatherIconID: String = "",
    val pressure: String = "",
    val humidity: String = "",
    val windSpeed: String = "",
    val windDir: String = "",
    val locationSearchEnabled: Boolean = false,
    val cityName: String = "",
    val appAlert: Pair<String, String>? = null

)
