package com.example.jpmweatherapp.ui

import android.location.Location
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.jpmweatherapp.model.WeatherInfoModel
import com.example.jpmweatherapp.util.http.SimpleHttpClient
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlin.math.roundToInt


/**
 *   Typically a reference to a repository is supplied to a viewmodel, but since this project is so simple,
 *   for brevity, an instance of an http client is directly provided instead of a repository
 *
 *   Also, I use this approach:
 *
 *   remote endpoint/api  ->
 *   http client(synchronous "get") ->
 *   binary payload ->
 *   JSON decoding into a data model class. Coroutines for asynchronous operations
 *
 *   This^ is just my personal preference and also based on feasibility and the endpoint implementation specifics
 *
 *   If it is absolutely necessary, I can happily rewrite the implementation to make use of Retrofit( which I already used in Java project btw)
 *   and/or RxJava for example
 *
 */
class WeatherViewModel(private val httpClient: SimpleHttpClient) : ViewModel() {

    var cityStateName by mutableStateOf("")
        private set

    private val _widgetsState = MutableStateFlow(UiWidgetState())
    val widgetsState: StateFlow<UiWidgetState> = _widgetsState.asStateFlow()

    private var location: Location? = null

    fun updateCityName(cityName: String) {
        cityStateName = cityName
    }

    fun resetErrorMessage() {
        _widgetsState.update { state -> state.copy(appAlert = null) }
    }

    fun clearWeatherInfo() {
        _widgetsState.update { state ->
            state.copy(
                cityName = "",
                humidity = "",
                pressure = "",
                weatherType ="",
                weatherDetails = "",
                windSpeed = "",
                windDir = "",
                currentTemperature = "",
                weatherIconID = "",
            )
        }
    }

    fun setCurrentLocation(loc: Location) {
        location = loc
        location?.let {
            _widgetsState.update { state ->
                state.copy(
                    locationSearchEnabled = true
                )
            }
        }
    }

    fun getCityWeather(fUseLocation: Boolean) {
        viewModelScope.launch(context = Dispatchers.IO) {

            val params = if (fUseLocation && location != null) {
                mapOf(
                    "lat" to "${location!!.latitude.toFloat()}",
                    "lon" to "${location!!.longitude.toFloat()}",
                    "units" to "Metric"
                )
            } else {
                mapOf(
                    "q" to "$cityStateName,US",
                    "units" to "Metric"
                )
            }

            val (payload, errMsg) = httpClient.doGet(
                "https://api.openweathermap.org/data/2.5/weather",
                params
            )
            if (payload == null) {
                val errorMessage = if (errMsg.contains("404")) "City $cityStateName is not found."
                else errMsg
                _widgetsState.update { state -> state.copy(appAlert = "Error" to errorMessage) }
            } else {
                try {
                    val weatherInfoModel =
                        (Gson().fromJson(String(payload), WeatherInfoModel::class.java))
                            ?: throw Exception("invalid file content")

                    val winDirMap = mapOf(
                        0..11 to "N",
                        12..33 to "NNE",
                        34..57 to "NE",
                        57..79 to "ENE",
                        79..102 to "E",
                        102..124 to "ESE",
                        125..147 to "SE",
                        147..167 to "SSE",
                        167..191 to "S",
                        192..213 to "SSW",
                        213..238 to "SW",
                        236..258 to "WSW",
                        258..281 to "W",
                        281..303 to "WNW",
                        303..326 to "NW",
                        326..348 to "NNW",
                        348..360 to "N",
                    )

                    var dirVerbose = "N/A"
                    for (range in winDirMap) {
                        if (weatherInfoModel.windInfo.direction in range.key) {
                            dirVerbose = range.value
                            break
                        }

                    }

                    _widgetsState.update { state ->
                        state.copy(
                            cityName = weatherInfoModel.cityName,
                            humidity = weatherInfoModel.weatherDetails.humidity.toString(),
                            pressure = weatherInfoModel.weatherDetails.pressure.toString(),
                            weatherType = weatherInfoModel.weatherInfo[0].main,
                            weatherDetails = weatherInfoModel.weatherInfo[0].description,
                            windSpeed = weatherInfoModel.windInfo.speed.toString(),
                            windDir = dirVerbose,
                            currentTemperature = weatherInfoModel.weatherDetails.currentTemp.roundToInt()
                                .toString(),
                            weatherIconID = weatherInfoModel.weatherInfo[0].iconID
                        )
                    }

                } catch (error: Exception) {
                    _widgetsState.update { state -> state.copy(appAlert = "Error" to "Internal error occurred, reason: ${error.message}") }
                }
            }
        }
    }
}