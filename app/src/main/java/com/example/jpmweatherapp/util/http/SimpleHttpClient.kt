package com.example.jpmweatherapp.util.http

import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import java.util.concurrent.TimeUnit


private val okHttpClient = OkHttpClient()

private const val DEFAULT_CONNECT_TIMEOUT_SECONDS: Long = 10
private const val DEFAULT_READ_TIMEOUT_SECONDS: Long = 60

class SimpleHttpClient(val authToken: String) {
    
    fun doGet(resourceUrl: String, params: Map<String, String>): Pair<ByteArray?, String> {
        var errorMessage = ""
        var payload: ByteArray? = null

        val sessionClientBuilder = okHttpClient.newBuilder()
            .readTimeout(DEFAULT_READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .connectTimeout(DEFAULT_CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .authenticator { _, response ->
                val requestBuilder = response.request.newBuilder()
                requestBuilder.addHeader("Accept", "application/json")
                requestBuilder.build()
            }

        val sessionClient = sessionClientBuilder.build()
        val httpBuilder: HttpUrl.Builder = resourceUrl.toHttpUrl().newBuilder()
        for (param in params) {
            httpBuilder.addQueryParameter(param.key, param.value)
        }
        httpBuilder.addQueryParameter("appid", authToken)

        val request = Request.Builder()
            .url(httpBuilder.build())
            .build()

        try {
            sessionClient.newCall(request).execute().use { response ->
                if (!response.isSuccessful) throw Exception("Unexpected code received: ${response.code}")
                payload = response.body?.bytes()
            }
        } catch (error: Exception) {
            errorMessage = error.message ?: error.javaClass.name
        }

        return payload to errorMessage
    }
}