package com.example.jpmweatherapp.ui

import com.example.jpmweatherapp.util.http.SimpleHttpClient
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.*
import okio.buffer
import okio.sink
import okio.source
import org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.File

class WeatherViewModelTest {



    @Before
    fun setUp() = MockKAnnotations.init(this, relaxUnitFun = true)


    @After
    fun tearDown() = unmockkAll()

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `Get Weather Test`( ) =runTest{

        val samplePayload = "{\"coord\":{\"lon\":-122.065,\"lat\":37.9063},\n" +
                "\t\"weather\":[{\"id\":501,\"main\":\"Rain\",\"description\":\"moderate rain\",\"icon\":\"10n\"}],\n" +
                "\t\"base\":\"stations\",\n" +
                "\t\"main\":{\"temp\":7.26,\"feels_like\":3.89,\"temp_min\":5.42,\"temp_max\":8.87,\"pressure\":1013,\"humidity\":78},\n" +
                "\t\"visibility\":10000,\n" +
                "\t\"wind\":{\"speed\":5.66,\"deg\":250},\n" +
                "\t\"rain\":{\"1h\":1.97},\n" +
                "\t\"clouds\":{\"all\":0},\n" +
                "\t\"dt\":1677551039,\n" +
                "\t\"sys\":{\"type\":2,\"id\":2003201,\"country\":\"US\",\"sunrise\":1677508985,\"sunset\":1677549556},\n" +
                "\t\"timezone\":-28800,\n" +
                "\t\"id\":5406990,\n" +
                "\t\"name\":\"San Francisco\",\n" +
                "\t\"cod\":200}"

        mockkConstructor(SimpleHttpClient::class)

        every { anyConstructed<SimpleHttpClient>().doGet( any(), any()) } answers {
            Pair(samplePayload.toByteArray(), "" )
        }

        val wvm = WeatherViewModel(SimpleHttpClient("123"))
        wvm.getCityWeather(false)
        advanceUntilIdle()
        assertFalse(wvm.widgetsState.value.cityName == "San Francisco")
    }
}