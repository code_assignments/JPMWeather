package com.example.jpmweatherapp.model

import com.google.gson.annotations.SerializedName

data class WeatherInfoModel(

    @SerializedName("weather")
    var weatherInfo: List<WeatherDescription>,

    @SerializedName("main")
    var weatherDetails: WeatherMain,

    @SerializedName("wind")
    var windInfo: WindInfo,

    @SerializedName("name")
    var cityName: String
)


data class WeatherMain(

    @SerializedName("temp")
    var currentTemp: Float = 0f,

    @SerializedName("pressure")
    var pressure: Int = 0,

    @SerializedName("humidity")
    var humidity: Int = 0,

    )


data class WindInfo(

    @SerializedName("speed")
    var speed: Float = 0f,

    @SerializedName("deg")
    var direction: Int = 0,

    )


data class WeatherDescription(

    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("main")
    var main: String = "",

    @SerializedName("description")
    var description: String = "",

    @SerializedName("icon")
    var iconID: String = "",
)
