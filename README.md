
This is a simple app for Android to query and display current weather in the specified city or based on current location.

It has been fully tested on a tablet with API 27 and works just fine, pretty much according to the requirements.

A video of the working app is included for reference in the project, refer to the file: jpm_weather_app.mp4 - this is a good demonstration of
the app features

This app is written in Kotlin/Compose. The app is very simple, so the code is pretty much self explanatory, 
but some comments have been added.

Please DO let me know, if I might have forgotten some features/requirements or improvements - I will be happy to add them in no time!

=====

Also, since, it was requested to code in java( or both), I have also quickly put together another very basic
project, written fully in Java with a standard stack( MVVM and Retrofit) It can be found here:
https://gitlab.com/code_assignments/jpmweather_java.git
( it just shows a five day weather forecast in a form of a scrollable list for a hard-coded city( San Francisco)  )